package ru.tsc.ichaplygina.taskmanager.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public final class CommandDescriptionEmptyException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Error! Command description cannot be empty.";

    public CommandDescriptionEmptyException() {
        super(MESSAGE);
    }

}
